package com.errigal.network;

/**
 * A network element is an element on the network which has a name, such as a Carrier, a Hub or a Node.
 */
public class NetworkElement {

    private String name;

    public NetworkElement(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }
}
