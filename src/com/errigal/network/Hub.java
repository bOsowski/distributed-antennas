package com.errigal.network;

import com.errigal.utils.Utils;

import java.util.HashMap;

public class Hub extends NetworkDevice{

    private HashMap<String, Node> nodesByNames = new HashMap<>();

    public Hub(String name, String ID) {
        super(name, ID);
    }

    public HashMap<String, Node> getNodesByNames() {
        return nodesByNames;
    }

    @Override
    public String toString(){
        StringBuilder nodesString = new StringBuilder();
        nodesString.append("          ");
        if(nodesByNames.values().size() == 0) nodesString.append(Utils.ANSI_CYAN+"none\n"+Utils.ANSI_RESET);
        for(Node node: nodesByNames.values()){
            nodesString.append(node.toString()+" | ");
        }
        return "Hub: "+getName()+"(ID:"+getID()+")"+Utils.ANSI_YELLOW+"\n             Nodes: \n"+Utils.ANSI_RESET+"      "+nodesString+"\n";
    }

    @Override
    public void addAlarm(AlarmType type){
        super.addAlarm(type);
        if(type.equals(AlarmType.UNIT_UNAVAILABLE)){
            for(Node node: getNodesByNames().values()){
                node.addAlarm(AlarmType.UNIT_UNAVAILABLE);
            }
        }
    }

    public String printAlarmStatus() {
        StringBuilder builder = new StringBuilder();
        if(getAmountOfTotalAlarms() > 0){
            builder.append(Utils.ANSI_RED);
        }
        builder.append(getName()+"(ID:"+getID()+")"+"'s alarms:\n");
        for(Alarm alarm: getAlarms().values()){
            if(alarm.getAmount() > 0){
                builder.append(Utils.ANSI_PURPLE+"            "+"("+alarm.getAmount()+")Alarm Name: "+alarm.type() +"  suggested remedy: "+alarm.getSuggestedRemedy()+Utils.ANSI_RESET+"\n");
            }
        }
        for(Node node: getNodesByNames().values()){

            builder.append("                      "+node.printAlarmStatus());

        }
        return builder.toString();
    }
}
