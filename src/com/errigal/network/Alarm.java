package com.errigal.network;

public class Alarm{

    private long amount = 0;
    private final AlarmType type;

    public Alarm(AlarmType type) {
        this.type = type;
    }

    public String getSuggestedRemedy() {
        return type.getRemedy();
    }

    public void incrementAmount(){
        amount++;
    }

    public long getAmount(){
        return amount;
    }

    public void removeAlarm(){
        this.amount = 0;
    }

    public AlarmType type(){
        return type;
    }

    public String toString(){
        return "("+amount+")"+type;
    }

}
