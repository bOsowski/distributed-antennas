package com.errigal.network;

public enum AlarmType{
    UNIT_UNAVAILABLE("Unit Unavaiable", "Restart it"), OPTICAL_LOSS("Optical Loss", "Fix the cables"), DARK_FIBRE("Dark Fibre", "Paint the fibre white!"), POWER_OUTAGE("Power Outage", "Contact the electricity provider");

    private String type;
    private String remedy;

    AlarmType(String type, String remedy){
        this.remedy = remedy;
        this.type = type;
    }

    public String type(){
        return type;
    }

    public String getRemedy() {
        return remedy;
    }
}