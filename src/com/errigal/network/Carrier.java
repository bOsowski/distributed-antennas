package com.errigal.network;

import com.errigal.utils.Utils;

import java.util.HashMap;

public class Carrier extends NetworkElement{

    private HashMap<String, Hub> hubsByNames = new HashMap<>();

    public Carrier(String name) {
        super(name);
    }

    public HashMap<String, Hub> getHubsByNames() {
        return hubsByNames;
    }

    @Override
    public String toString(){
        StringBuilder hubsString = new StringBuilder();
        for(Hub hub: hubsByNames.values()){
            hubsString.append("          "+hub.toString());
        }
        if(hubsByNames.values().size() == 0) return "Carrier: "+getName()+ Utils.ANSI_GREEN+"\n    Hubs: \n"+Utils.ANSI_RESET+Utils.ANSI_CYAN+"      none\n"+Utils.ANSI_RESET;
        return "Carrier: "+getName()+ Utils.ANSI_GREEN+"\n    Hubs: \n"+Utils.ANSI_RESET+hubsString;
    }


    public String printAlarmStatus(){
        StringBuilder builder = new StringBuilder();
        builder.append(getName()+"'s alarms:\n");
       for(Hub hub: getHubsByNames().values()){
            builder.append("     "+hub.printAlarmStatus());
       }
        return builder.toString();
    }

}
