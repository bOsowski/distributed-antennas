package com.errigal.network;

public enum DeviceType {
    Hub, Node
}
