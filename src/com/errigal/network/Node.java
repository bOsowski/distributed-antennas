package com.errigal.network;

import com.errigal.utils.Utils;

public class Node extends NetworkDevice{

    public Node(String name, String ID) {
        super(name, ID);
    }

    @Override
    public String toString(){
        return "Node: "+getName()+"(ID:"+getID()+")";
    }

    public String printAlarmStatus() {
        if(getAmountOfTotalAlarms() == 0){
            return "";
        }
        StringBuilder builder = new StringBuilder();
        if(getAmountOfTotalAlarms() > 0){
            builder.append(Utils.ANSI_YELLOW);
        }
        builder.append(""+getName()+"(ID: "+getID()+")"+"'s alarms:\n"+Utils.ANSI_RESET);
        for(Alarm alarm: getAlarms().values()){
            if(alarm.getAmount() > 0){
                builder.append(Utils.ANSI_PURPLE+"                                "+"("+alarm.getAmount()+")Alarm Name: "+alarm.type() +"  suggested remedy: "+alarm.getSuggestedRemedy()+Utils.ANSI_RESET+"\n");
            }
        }
        return builder.toString();
    }
}
