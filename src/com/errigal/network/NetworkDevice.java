package com.errigal.network;

import java.util.HashMap;

/**
 * A network device is an actual device on the network which has a name and an ID and can have an alarm.
 */
public class NetworkDevice extends NetworkElement{

    private String ID;
    private long amountOfTotalAlarms = 0;
    private HashMap<AlarmType, Alarm> alarms = new HashMap<>();
    private NetworkElement parent;

    public NetworkDevice(String name, String ID){
        super(name);
        this.ID = ID;
        alarms.put(AlarmType.UNIT_UNAVAILABLE, new Alarm(AlarmType.UNIT_UNAVAILABLE));
        alarms.put(AlarmType.OPTICAL_LOSS, new Alarm(AlarmType.OPTICAL_LOSS));
        alarms.put(AlarmType.DARK_FIBRE, new Alarm(AlarmType.DARK_FIBRE));
        alarms.put(AlarmType.POWER_OUTAGE, new Alarm(AlarmType.POWER_OUTAGE));
    }

    public String getID(){
        return ID;
    }

    public HashMap<AlarmType, Alarm> getAlarms() {
        return alarms;
    }


    public void addAlarm(AlarmType type){
        amountOfTotalAlarms++;
        alarms.get(type).incrementAmount();
    }

    public long getAmountOfTotalAlarms(){
        return amountOfTotalAlarms;
    }

    public void addAmountOfTotalAlarms(long amount){
        amountOfTotalAlarms += amount;
    }

    public void loadAlarms(HashMap<AlarmType, Alarm> alarms){
        this.alarms = alarms;
    }


    public NetworkElement getParent() {
        return parent;
    }

    public void setParent(NetworkElement parent) {
        this.parent = parent;
    }
}
