package com.errigal;

import com.errigal.network.*;
import com.errigal.utils.Utils;

import java.util.HashMap;

public class Controller {

    Network network = new Network();

    public Network getNetwork() {
        return network;
    }

    /**
     *
     * @param carrierName
     * @return the boolean outcome of the carrier addition.
     */
    public boolean addCarrier(String carrierName) {
        if(network.getCarriers().containsKey(carrierName)){
            return false;
        }
        else{
            network.getCarriers().put(carrierName, new Carrier(carrierName));return true;
        }
    }

    /**
     *
     * @param carrierName
     * @return the boolean outcome of the carrier removal.
     */
    public boolean removeCarrier(String carrierName) {
        if(!network.getCarriers().containsKey(carrierName)) return false;
        Carrier carrier = network.getCarriers().get(carrierName);
        HashMap<String,Hub> copy = (HashMap<String,Hub>) carrier.getHubsByNames().clone();
        for(Hub hub:copy.values()){
            if(!removeHub(hub)) return false;
        }
        network.getCarriers().remove(carrierName);
        return true;
    }

    /**
     *
     * @param hubName
     * @param hubID
     * @return the boolean outcome of the hub addition.
     */
    public boolean addHub(Carrier carrier, String hubName, String hubID) {
        //ensure the hub name is unique within the carrier and the hub id is unique within the system.
            if(!carrier.getHubsByNames().containsKey(hubName) && !network.getHubsByIDs().containsKey(hubID)){
                //create the new hub.
                Hub hub = new Hub(hubName, hubID);
                //set the hub's parent to the holding carrier.
                hub.setParent(carrier);
                carrier.getHubsByNames().put(hubName, hub);
                network.getHubsByIDs().put(hubID, hub);
                return true;
            }
        Utils.printNotFoundMessage();
        return false;
    }

    /**
     *
     * @return the boolean outcome of the hub removal.
     */
    public boolean removeHub(Hub hub) {
        if(hub == null || !getNetwork().getHubsByIDs().containsKey(hub.getID())) return false;
            //remove the child nodes from hub.
            removeAllHubNodesFromNetwork(hub);
            //remove the hub reference from parent.
            Carrier parentCarrier = (Carrier) hub.getParent();
            parentCarrier.getHubsByNames().remove(hub.getName());
            //finally remove the hub reference from the network.
            network.getHubsByIDs().remove(hub.getID());
            return true;
    }

    public void removeAllHubNodesFromNetwork(Hub hub){
        for (Node node : hub.getNodesByNames().values()) {
            network.getNodesByIDs().remove(node.getID());
        }
    }

    /**
     *
     * @param nodeName
     * @param nodeID
     * @return the boolean outcome of the node addition.
     */
    public boolean addNode(Hub hub, String nodeName, String nodeID) {
        //if the hubID exists in the network and nodeID is unique and the name is unique within the hub..
            if(hub!= null && !network.getNodesByIDs().containsKey(nodeID) && !hub.getNodesByNames().containsKey(nodeName)){
                Node node = new Node(nodeName, nodeID);
                node.setParent(network.getHubsByIDs().get(hub.getID()));
                hub.getNodesByNames().put(nodeName, node);
                network.getNodesByIDs().put(nodeID, node);
                return true;
            }
        Utils.printNotFoundMessage();
        return false;
    }

    /**
     *
     * @return the boolean outcome of the node deletion.
     */
    public boolean removeNode(Node node) {
        try{
            Hub hub = (Hub)node.getParent();
            network.getNodesByIDs().remove(node.getID());
            hub.getNodesByNames().remove(node.getName());
            return true;
        }catch(NullPointerException e){
            Utils.printNotFoundMessage();
            return false;
        }
    }
    /**
     * Prints the entire network except of alarms to the console.
     */
    public void listEntireNetwork(){
        System.out.println("Listing the entire network..:\n");
        for(Carrier carrier: network.getCarriers().values()){
            Utils.printLineSeparator();
            System.out.println(carrier.toString());
        }
        Utils.printEndLineSeparator();
    }

    /**
     * Prints all the alarms on the network
     */
    public void printAlarmStatus(){
        System.out.println("Listing the status of the network..:\n");
        for(Carrier carrier: network.getCarriers().values()){
            Utils.printLineSeparator();
            System.out.println(carrier.printAlarmStatus());
        }
        Utils.printEndLineSeparator();
    }

    /**
     * prints all the carriers
     */
    public void printTheCarriers(){
        System.out.println("Listing the carriers on the network.:\n");
        for(String carrier: network.getCarriers().keySet()){
            System.out.println("    "+carrier);
        }
        System.out.println();
    }

    public boolean listHubsWithinACarrier(Carrier carrier){
        if(carrier == null)return false;
        System.out.println("Printing all hubs within "+carrier.getName()+"..:");
        for(Hub hub: carrier.getHubsByNames().values()){
            System.out.println("Hub name: "+hub.getName()+ "|Hub ID: "+hub.getID());
        }
        System.out.println();
        return true;
    }

    public boolean listNodesOnAHub(Hub hub){
        System.out.println("Printing all nodes within hub with ID = '"+hub.getID()+"'..:");
        for(Node node: hub.getNodesByNames().values()){
            System.out.println("Node name: "+node.getName()+"|Node ID: "+node.getID());
        }
        System.out.println();
        return true;
    }

    public boolean changeNameOfCarrier(Carrier carrier, String newName){
        //make sure the carrier exists, and the new name doesn't already exists.
        if(carrier == null || network.getCarriers().containsKey(newName)){
            return false;
        }
        network.getCarriers().remove(carrier.getName());
        carrier.setName(newName);
        network.getCarriers().put(newName, carrier);
        return true;
    }

    public boolean changeNameOfHub(Hub hub, String newHubName) throws NullPointerException{
        Carrier parentCarrier = (Carrier)hub.getParent();
        if(parentCarrier.getHubsByNames().containsKey(newHubName))return  false;
        parentCarrier.getHubsByNames().remove(hub.getName());
        hub.setName(newHubName);
        parentCarrier.getHubsByNames().put(newHubName, hub);
        return true;
    }

    public boolean changeNameOfNode(Node node, String newNodeName){
        Hub hub = (Hub)node.getParent();
        if(hub.getNodesByNames().containsKey(newNodeName)) return false;

        hub.getNodesByNames().remove(node.getName());
        node.setName(newNodeName);
        hub.getNodesByNames().put(newNodeName, node);

        return true;
    }

    public boolean printAlarmStatusForHubs(String hubIDs){
        try{
            String[] splitString = hubIDs.split(" ");
            for(String hubID: splitString){
                Hub hub = network.getHubsByIDs().get(hubID);
                System.out.println("Alarms on hub '"+hub.getName()+"(ID:"+hub.getID()+")':");
                for(Alarm alarm: hub.getAlarms().values()){
                    String colourCodePrefix = Utils.ANSI_RED;
                    if(alarm.getAmount() == 0){
                        colourCodePrefix = Utils.ANSI_GREEN;
                    }
                    System.out.println("    "+colourCodePrefix+alarm.toString()+Utils.ANSI_RESET);
                }
                System.out.println();
            }
            return true;
        }catch (NullPointerException e){
            Utils.printNotFoundMessage();
        }
        return false;
    }

    public boolean printAlarmStatusForNodes(String nodeIDs){
        try{
            String[] splitString = nodeIDs.split(" ");
            for(String nodeID: splitString){
                Node node = network.getNodesByIDs().get(nodeID);
                System.out.println("Alarms on node '"+node.getName()+"(ID:"+node.getID()+")':");
                for(Alarm alarm: node.getAlarms().values()){
                    String colourCodePrefix = Utils.ANSI_RED;
                    if(alarm.getAmount() == 0){
                        colourCodePrefix = Utils.ANSI_GREEN;
                    }
                    System.out.println("    "+colourCodePrefix+alarm.toString()+Utils.ANSI_RESET);
                }
                System.out.println();
            }
            return true;
        }catch (NullPointerException e){
            Utils.printNotFoundMessage();
        }
        return false;
    }

    public boolean viewSpecificRemediesForHubAlarms(Hub hub){
        try{
            System.out.println("Suggested remedies for hub '"+hub.getName()+" (ID:"+hub.getID()+") :");
            for(Alarm alarm: hub.getAlarms().values()){
                if(alarm.getAmount() > 0){
                    System.out.println("    "+Utils.ANSI_GREEN+alarm.getSuggestedRemedy()+Utils.ANSI_RESET);
                }
            }
            System.out.println();
            return true;
        }catch (NullPointerException e){
            return false;
        }
    }

    public boolean viewSpecificRemediesForNodeAlarms(Node node){
        try{
            System.out.println("Suggested remedies for node '"+node.getName()+" (ID:"+node.getID()+") :");
            for(Alarm alarm: node.getAlarms().values()){
                if(alarm.getAmount() > 0){
                    System.out.println("    "+Utils.ANSI_GREEN+alarm.getSuggestedRemedy()+Utils.ANSI_RESET);
                }
            }
            System.out.println();
            return true;
        }catch (NullPointerException e){
            return false;
        }
    }

    public boolean clearSpecificAlarmOnDevice(NetworkDevice device, AlarmType alarmType){
        if(device == null) return false;
        device.getAlarms().get(alarmType).removeAlarm();
        return true;
    }

    public boolean clearAllAlarmsOnDevice(NetworkDevice device){
        for(Alarm alarm: device.getAlarms().values()){
            alarm.removeAlarm();
        }
        return true;
    }
}
