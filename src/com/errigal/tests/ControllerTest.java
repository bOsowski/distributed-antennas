package com.errigal.tests;

import com.errigal.Controller;
import com.errigal.network.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ControllerTest {

    Controller controller;
    int hubsPerCarrier = 9;
    int nodesPerHub = 81;

    @Before
    @Test
    public void additionTest() {
        controller = new Controller();

        assertTrue(controller.addCarrier("Verizon"));
        assertTrue(controller.addCarrier("Tmobile"));
        assertTrue(controller.addCarrier("Vodafone"));
        assertFalse(controller.addCarrier("Vodafone"));
        assertFalse(controller.addCarrier("Vodafone"));


        for(Carrier carrier: controller.getNetwork().getCarriers().values()){
            for(int i = 0; i<hubsPerCarrier; i++){
                String localHubId = carrier.getName()+"Hub"+i;
                assertTrue(controller.addHub(carrier, "Hub"+i, localHubId));
                assertFalse(controller.addHub(carrier, "Hub"+i, localHubId));
                Hub localHub = controller.getNetwork().getHubsByIDs().get(localHubId);
                assertNotEquals(null, localHub.getParent());
                for(int j = 0;j<nodesPerHub; j++){
                    String localNodeId = carrier.getName()+ localHub.getName()+"Node"+j;
                    assertTrue(controller.addNode(localHub, "Node"+j,localNodeId));
                    assertFalse(controller.addNode(localHub, "Node"+j,localNodeId));
                    assertNotEquals(null, controller.getNetwork().getNodesByIDs().get(localNodeId).getParent());
                }
            }
        }
        assertFalse(controller.addNode(null, "newName","newId"));
        assertEquals(controller.getNetwork().getCarriers().values().size(), 3);
        assertEquals(controller.getNetwork().getHubsByIDs().size(), controller.getNetwork().getCarriers().values().size()*hubsPerCarrier);
        assertEquals(controller.getNetwork().getNodesByIDs().size(), controller.getNetwork().getCarriers().values().size()*hubsPerCarrier*nodesPerHub);


        //every hub should have 4 total alarms (1 each)
        //every node should have 4+1 total alarms (1 from parent, and 1 each).
        for(Carrier carrier: controller.getNetwork().getCarriers().values()){
            for(AlarmType type: AlarmType.values()){
                for(Hub hub: carrier.getHubsByNames().values()){
                    hub.addAlarm(type);
                    for(Node node: hub.getNodesByNames().values()){
                        node.addAlarm(type);
                        if(type == AlarmType.UNIT_UNAVAILABLE){
                            assertEquals(2,node.getAlarms().get(type).getAmount());
                        }
                        else{
                            assertEquals(1,node.getAlarms().get(type).getAmount());
                        }
                    }
                    assertEquals(1,hub.getAlarms().get(type).getAmount());
                }
            }
        }
    }

    @Test
    public void testMassDeletions(){
        assertTrue(controller.removeNode(controller.getNetwork().getNodesByIDs().get("VerizonHub1Node1")));
        assertTrue(controller.removeHub(controller.getNetwork().getHubsByIDs().get("TmobileHub2")));
        assertTrue(controller.removeCarrier("Verizon"));
        assertTrue(controller.removeCarrier("Tmobile"));
        assertTrue(controller.removeCarrier("Vodafone"));
        assertFalse(controller.removeCarrier("Verizon"));
        assertFalse(controller.removeHub(null));
        assertFalse(controller.removeNode(null));

        assertEquals(0, controller.getNetwork().getCarriers().size());
        assertEquals(0, controller.getNetwork().getCarriers().size());
        assertEquals(0, controller.getNetwork().getHubsByIDs().size());
        assertEquals(0, controller.getNetwork().getNodesByIDs().size());
    }

    @Test
    public void testNodeDeletions(){
        assertTrue(controller.removeNode(controller.getNetwork().getNodesByIDs().get("VerizonHub1Node1")));
        assertFalse(controller.removeNode(controller.getNetwork().getNodesByIDs().get("VerizonHub1Node1")));
    }

    @Test
    public void testHubDeletions(){
        assertTrue(controller.removeHub(controller.getNetwork().getHubsByIDs().get("VerizonHub1")));
        assertFalse(controller.removeHub(controller.getNetwork().getHubsByIDs().get("VerizonHub1")));
    }

    @Test
    public void testCarrierDeletions(){
        assertTrue(controller.removeCarrier("Verizon"));
        assertTrue(controller.removeCarrier("Tmobile"));
        assertTrue(controller.removeCarrier("Vodafone"));
        assertFalse(controller.removeCarrier("Verizon"));
        assertFalse(controller.removeCarrier("NonExisting"));
    }


    @Test
    public void testAllAlarmDeletionOnHubs(){
        NetworkDevice device = controller.getNetwork().getHubsByIDs().get("VerizonHub0");
        assertTrue(controller.clearAllAlarmsOnDevice(device));
        for(AlarmType type: AlarmType.values()){
            assertEquals(0, device.getAlarms().get(type).getAmount());
        }
    }

    @Test
    public void testAllAlarmDeletionOnNodes(){
        NetworkDevice device = controller.getNetwork().getNodesByIDs().get("VerizonHub0Node0");
        assertTrue(controller.clearAllAlarmsOnDevice(device));
        for(AlarmType type: AlarmType.values()) {
            assertEquals(0, device.getAlarms().get(type).getAmount());
        }
    }


    @Test
    public void testAlarmDeletionHubs(){
        NetworkDevice device = controller.getNetwork().getHubsByIDs().get("TmobileHub2");
        for(AlarmType type: AlarmType.values()){
            assertNotEquals(0, device.getAlarms().get(type).getAmount());
            assertTrue(controller.clearSpecificAlarmOnDevice(device, type));
            assertEquals(0, device.getAlarms().get(type).getAmount());
        }
    }

    @Test
    public void testAlarmDeletionNodes(){
        NetworkDevice device = controller.getNetwork().getNodesByIDs().get("TmobileHub0Node0");
        for(AlarmType type: AlarmType.values()){
            assertNotEquals(0, device.getAlarms().get(type).getAmount());
            assertTrue(controller.clearSpecificAlarmOnDevice(device, type));
            assertEquals(0, device.getAlarms().get(type).getAmount());
        }
    }

    @Test
    public void testCarrierRenaming(){
        Carrier carrier = controller.getNetwork().getCarriers().get("Verizon");
        assertTrue(controller.changeNameOfCarrier(carrier, "NewVerizon"));
        assertFalse(controller.changeNameOfCarrier(null, "NewVerizon"));
        assertFalse(controller.getNetwork().getCarriers().containsKey("Verizon"));
        assertTrue(controller.getNetwork().getCarriers().containsKey("NewVerizon"));
    }

    @Test
    public void testHubRenaming(){
        Carrier carrier = controller.getNetwork().getCarriers().get("Verizon");
        Hub hub = controller.getNetwork().getHubsByIDs().get("VerizonHub1");
        assertTrue(controller.changeNameOfHub(hub, "newHub1"));
        assertFalse(carrier.getHubsByNames().containsKey("Verizon"));
        assertTrue(carrier.getHubsByNames().containsKey("newHub1"));
    }

    @Test
    public void testNodeRenaming(){
        Node node = controller.getNetwork().getNodesByIDs().get("VerizonHub1Node1");
        assertNotEquals(null, node);
        assertNotEquals(null, node.getParent());
        assertTrue(controller.changeNameOfNode(node, "newNode1"));
        assertFalse(((Hub)node.getParent()).getNodesByNames().containsKey("Hub1Node1"));
        assertTrue(((Hub)node.getParent()).getNodesByNames().containsKey("newNode1"));
    }

    @Test
    public void testNetworkElementListing(){
        Carrier carrier = controller.getNetwork().getCarriers().get("Verizon");
        Hub hub = controller.getNetwork().getHubsByIDs().get("VerizonHub1");

        controller.listHubsWithinACarrier(carrier);
        controller.listNodesOnAHub(hub);
        controller.printTheCarriers();
    }

    @Test
    public void testAlarmStatusPrintForHubs(){
        assertTrue(controller.printAlarmStatusForHubs("VerizonHub1"));
        assertFalse(controller.printAlarmStatusForHubs("sebset"));
    }

    @Test
    public void testAlarmStatusPrintForNodes(){
        assertFalse(controller.printAlarmStatusForNodes("dstbrtr"));
        assertTrue(controller.printAlarmStatusForNodes("VerizonHub1Node1"));
    }

    @Test
    public void testSpecificRemediesForHubs(){
        Hub hub = controller.getNetwork().getHubsByIDs().get("VerizonHub1");
        assertTrue(controller.viewSpecificRemediesForHubAlarms(hub));
        assertFalse(controller.viewSpecificRemediesForHubAlarms(null));
    }

    @Test
    public void testSpecificRemediesForNodes(){
        Node node = controller.getNetwork().getNodesByIDs().get("VerizonHub1Node1");
        assertTrue(controller.viewSpecificRemediesForNodeAlarms(node));
        assertFalse(controller.viewSpecificRemediesForNodeAlarms(null));
    }

    @Test
    public void testAlarmPrintout(){
        controller.printAlarmStatus();
    }

    @Test
    public void testNetworkPrintout() {
        controller.listEntireNetwork();
    }

}