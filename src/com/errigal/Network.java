package com.errigal;

import com.errigal.network.Carrier;
import com.errigal.network.Hub;
import com.errigal.network.Node;

import java.util.HashMap;

public class Network {

    private HashMap<String, Carrier> carriers = new HashMap<>();
    private HashMap<String, Hub> hubsByIDs = new HashMap<>();
    private HashMap<String, Node> nodesByIDs = new HashMap<>();

    public HashMap<String, Carrier> getCarriers() {
        return carriers;
    }

    public HashMap<String, Hub> getHubsByIDs() {
        return hubsByIDs;
    }

    public HashMap<String, Node> getNodesByIDs() {
        return nodesByIDs;
    }

}
