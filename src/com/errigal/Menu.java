package com.errigal;

import com.errigal.utils.Utils;
import java.util.Scanner;

public class Menu {

    Scanner scanner = new Scanner(System.in);
    InputManager inputManager = new InputManager(scanner);
    DataManager dataManager = new DataManager();

    public void displayMainMenu(){
        System.out.println("a) List the entire network.");
        System.out.println("b) View the alarm status of the network.");
        System.out.println("c) List the carriers.");
        System.out.println("d) List the hubs within a carrier.");
        System.out.println("e) List the nodes on a hub.");
        System.out.println("f) Add/remove a carrier.");
        System.out.println("g) Add/remove a hub.");
        System.out.println("h) Add/remove a node.");
        System.out.println("i) Change name of carrier/hub/node.");
        System.out.println("j) Create an alarm on a hub/node.");
        System.out.println("k) View the alarm status of hubs/nodes");
        System.out.println("l) View the suggested remedies for a hub/node");
        System.out.println("m) Clear specific or all alarms on a hub/node");
        System.out.println("n) Export the network to a JSON file.");
        System.out.println("o) Import a network via a JSON file.");
        System.out.println("0) Quit Application.");
    }

    public void run() {
        displayMainMenu();
        System.out.print("\n>>");
        char choice = scanner.next().toLowerCase().charAt(0);

        switch (choice) {
            case 'a':
                inputManager.controller.listEntireNetwork();
                break;
            case 'b':
                inputManager.controller.printAlarmStatus();
                break;
            case 'c':
                inputManager.controller.printTheCarriers();
                break;
            case 'd':
                inputManager.controller.listHubsWithinACarrier(inputManager.getCarrierFromUser());
                break;
            case 'e':
                inputManager.listNodesOnHub();
                break;
            case 'f':
                inputManager.addRemoveCarrier();
                break;
            case 'g':
                inputManager.addRemoveHub();
                break;
            case 'h':
                inputManager.addRemoveNode();
                break;
            case 'i':
                inputManager.changeName();
                break;
            case 'j':
                inputManager.createAlarm();
                break;
            case 'k':
                inputManager.viewAlarmStatus();
                break;
            case 'l':
                inputManager.viewSuggestedRemedies();
                break;
            case 'm':
                inputManager.clearSpecificOrAllAlarms();
                break;
            case 'n':
                dataManager.saveToFile("DataSave.json", inputManager.controller);
                break;
            case 'o':
                dataManager.readFromFile("DataSave.json", inputManager.controller);
                break;
            case '0':
                System.exit(0);
                break;
            default:
                Utils.printWrongInputMessage();
                break;
        }
        run();
    }
}
