package com.errigal.utils;

public class Utils {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static void printNotFoundMessage(){
        System.out.println(ANSI_YELLOW+"Error. Make sure the required element(s) are present in the database.\n"+ANSI_RESET);
    }

    public static void printWrongInputMessage(){
        System.out.println(ANSI_YELLOW+"WRONG INPUT.\n"+ANSI_RESET);
    }

    public static void printYellow(String message){
        System.out.println(ANSI_YELLOW+message+ANSI_RESET);
    }


    public static void printEndLineSeparator() {
        System.out.println("------------------------------END----------------------------------\n");
    }

    public static void printLineSeparator() {
        System.out.println("-------------------------------------------------------------------");
    }
}

