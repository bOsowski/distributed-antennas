package com.errigal;

import com.errigal.network.*;
import com.errigal.utils.Utils;

import java.util.Scanner;

public class InputManager {

    Controller controller = new Controller();
    Scanner scanner;

    InputManager(Scanner scanner){
        this.scanner = scanner;
    }

    /**
     *
     * @param of what network element? (eg.: carrier -> prints "Please enter the name of the carrier >>)
     * @return user input.
     */
    String getNameFromUser(String of){
        System.out.print("Please enter the name of the "+of+" >>");
        return scanner.next();
    }

    /**
     *
     * @param name
     * @return the carrier if found, else returns null.
     */
    Carrier getCarrierFromName(String name){
        Carrier carrier = controller.getNetwork().getCarriers().get(name);
        if(carrier == null){
            Utils.printNotFoundMessage();
        }
        return carrier;
    }

    /**
     *
     * @param of what network element? Similar to getNameFromUser, but with a different message.
     * @return
     */
    String getIdFromUser(String of){
        System.out.print("Please enter the ID of the "+of+" >>");
        return scanner.next();
    }

    /**
     * Asks user for the id, and tries to find the hub with the given id. Might return null if wrong input.
     * @return
     */
    Hub getHubByID(){
        Hub hub = controller.getNetwork().getHubsByIDs().get(getIdFromUser("hub"));
        if(hub == null){
            Utils.printNotFoundMessage();
        }
        return hub;
    }

    /**
     * Asks user to specify the name of the carrier, then asks to specify
     * @return
     */
    Hub getHubByName(){
        Carrier carrier = getCarrierFromName(getNameFromUser("carrier"));
        if(carrier == null){
            return null;
        }
        Hub hub = carrier.getHubsByNames().get(getNameFromUser("hub"));
        if(hub == null){
            Utils.printNotFoundMessage();
        }
        return hub;
    }

    Node getNodeByID(){
        Node node = controller.getNetwork().getNodesByIDs().get(getIdFromUser("node"));
        if(node == null){
            Utils.printNotFoundMessage();
        }
        return node;
    }

    Node getNodeByName(Hub hub) throws NullPointerException{
        if(hub == null)return null;
        Node node = hub.getNodesByNames().get(getNameFromUser("node"));
        if(node == null){
            Utils.printNotFoundMessage();
        }
        return node;
    }

    Carrier getCarrierFromUser(){
        return getCarrierFromName(getNameFromUser("carrier"));
    }

    Hub getHubFromUser(){
        System.out.println("How do you want to specify the hub?");
        System.out.println("    a) By hub ID.");
        System.out.println("    b) By carrier name and hub name.");
        System.out.print("\n>>");
        char choice = scanner.next().toLowerCase().charAt(0);

        switch(choice){
            case 'a':
                return getHubByID();    //this might return null, but user will get the not found message if it does
            case 'b':
                return getHubByName();  //same as above comment.
            default:
                Utils.printWrongInputMessage(); //wrong choice. return null and print the appropriate message
                return null;
        }
    }

    Node getNodeFromUser(){
        System.out.println("How do you want to specify the node?");
        System.out.println("    a) By node ID.");
        System.out.println("    b) By finding hub and then specifying node name.");
        System.out.print("\n>>");
        char choice = scanner.next().toLowerCase().charAt(0);

        switch(choice){
            case 'a':
                return getNodeByID();    //this might return null, but user will get the not found message if it does
            case 'b':
                return getNodeByName(getHubFromUser());  //same as above comment.
            default:
                Utils.printWrongInputMessage(); //wrong choice. return null and print the appropriate message
                return null;
        }
    }

    void addRemoveCarrier(){
        System.out.println("a) Add a carrier.");
        System.out.println("b) Remove a carrier.\n");

        System.out.print("\n>>");
        String carrierName;
        char choice = scanner.next().toLowerCase().charAt(0);
        switch (choice){
            case 'a':
                carrierName = getNameFromUser("carrier to add");
                if(controller.getNetwork().getCarriers().containsKey(carrierName)){
                    Utils.printYellow("This carrier already exists!");
                }
                else if(controller.addCarrier(carrierName)){
                    Utils.printYellow("Carrier successfully added.");
                }
                else{
                    System.err.println("ERROR!");
                }
                break;
            case 'b':
                carrierName = getNameFromUser("carrier to delete");
                if(controller.getNetwork().getCarriers().containsKey(carrierName)){
                    if(controller.removeCarrier(carrierName)){
                        Utils.printYellow("Carrier successfully removed!");
                    }
                    else{
                        System.err.println("Error!");
                    }
                }
                else{
                    Utils.printYellow("This carrier doesn't exist.");
                }
                break;
            default:
                Utils.printWrongInputMessage();
                break;
        }
        System.out.println();
    }

    void addRemoveHub(){
        System.out.println("a) Add a hub.");
        System.out.println("b) Remove a hub.");
        System.out.print("\n>>");
        char choice = scanner.next().toLowerCase().charAt(0);

        switch (choice){
            case 'a':
                Carrier carrier = getCarrierFromUser();
                if(carrier == null) return;
                String name = getNameFromUser("hub to add");
                if(carrier.getHubsByNames().containsKey(name)){
                    Utils.printYellow("This hub name already exists within the carrier.");
                    return;
                }
                String id = getIdFromUser("hub to add");
                if(controller.getNetwork().getHubsByIDs().containsKey(id)){
                    Utils.printYellow("This hub id already exists within the network.");
                    return;
                }
                if(controller.addHub(carrier, name, id)){
                    Utils.printYellow("Hub successfully added!");
                }
                else{
                    System.err.println("Error!");
                }
                break;
            case 'b':
                Hub hub = getHubFromUser();
                if(hub == null) return;
                if(controller.removeHub(hub)){
                    Utils.printYellow("Hub successfully removed!");
                }else{
                    System.err.println("Error!");
                }
                break;
            default:
                Utils.printWrongInputMessage();
                break;
        }
        System.out.println();
    }



    void addRemoveNode(){
        System.out.println("a) Add a node.");
        System.out.println("b) Remove a node.");
        System.out.print("\n>>");
        char choice = scanner.next().toLowerCase().charAt(0);

        switch (choice){
            case 'a':
                Hub hub = getHubFromUser();
                if(hub == null) return;
                String name = getNameFromUser("node to add");
                if(hub.getNodesByNames().containsKey(name)){
                    Utils.printYellow("This node name already exists within the hub.");
                    return;
                }
                String id = getIdFromUser("node to add");
                if(controller.getNetwork().getNodesByIDs().containsKey(id)){
                    Utils.printYellow("This node id already exists within the network.");
                    return;
                }
                if(controller.addNode(hub, name, id)){
                    Utils.printYellow("Node successfully added!");
                }
                else{
                    System.err.println("Error!");
                }
                break;
            case 'b':
                Node node = getNodeFromUser();
                if(node == null) return;
                if(controller.removeNode(node)){
                    Utils.printYellow("Node successfully removed!");
                }
                else{
                    System.err.println("Error!");
                }
                break;
            default:
                Utils.printWrongInputMessage();
                break;
        }
        System.out.println();
    }

    void listNodesOnHub(){
        Hub hub = getHubFromUser();
        if(hub == null) return;
        if(!controller.listNodesOnAHub(hub)){
            System.err.println("Error!");
        }
    }

    void changeName(){
        System.out.println("What would you like to change the name of?");
        System.out.println("    a) Carrier.");
        System.out.println("    b) Hub.");
        System.out.println("    c) Node.");
        System.out.print("\n>>");
        char choice = scanner.next().toLowerCase().charAt(0);
        String name;
        switch(choice){
            case 'a':
                Carrier carrier = getCarrierFromUser();
                if(carrier == null) return;
                name = getNameFromUser("new carrier name");
                if(controller.getNetwork().getCarriers().containsKey(name)){
                    Utils.printYellow("This name is already taken!");
                    return;
                }
                else{
                    if(controller.changeNameOfCarrier(carrier, name)){
                        Utils.printYellow("Successfully changed the carrier's name!");
                    }
                    else{
                        System.err.println("Error!");
                    }
                }
                break;
            case 'b':
                Hub hub = getHubFromUser();
                if(hub == null) return;
                name = getNameFromUser("new hub name");
                if(((Carrier)hub.getParent()).getHubsByNames().containsKey(name)){
                    Utils.printYellow("This name is already taken!");
                    return;
                }
                else{
                    if(controller.changeNameOfHub(hub, name)){
                        Utils.printYellow("Successfully changed the hub's name!");
                    }
                    else{
                        System.err.println("Error!");
                    }
                }
                break;
            case 'c':
                Node node = getNodeFromUser();
                if(node == null) return;
                name = getNameFromUser("new node name");
                if(((Hub)node.getParent()).getNodesByNames().containsKey(name)){
                    Utils.printYellow("This name is already taken!");
                    return;
                }
                else{
                    if(controller.changeNameOfNode(node, name)){
                        Utils.printYellow("Successfully changed the node's name!");
                    }
                    else{
                        System.err.println("Error!");
                    }
                }
                break;
            default:
                Utils.printWrongInputMessage();
                break;
        }
        System.out.println();
    }

    AlarmType getAlarmFromUser(){
        System.out.println("Which alarm type?");
        System.out.println("    a) "+AlarmType.POWER_OUTAGE);
        System.out.println("    b) "+AlarmType.UNIT_UNAVAILABLE);
        System.out.println("    c) "+AlarmType.DARK_FIBRE);
        System.out.println("    d) "+AlarmType.OPTICAL_LOSS);
        System.out.print("\n>>");
        char choice = scanner.next().toLowerCase().charAt(0);

        switch (choice){
            case 'a': return AlarmType.POWER_OUTAGE;
            case 'b': return AlarmType.UNIT_UNAVAILABLE;
            case 'c': return AlarmType.DARK_FIBRE;
            case 'd': return AlarmType.OPTICAL_LOSS;
            default:
                Utils.printWrongInputMessage();
                return null;
        }
    }

    DeviceType getDeviceType(){
        System.out.println("Specify device type:");
        System.out.println("    a) Hub");
        System.out.println("    b) Node");
        System.out.print("\n>>");
        char choice = scanner.next().toLowerCase().charAt(0);
        switch (choice){
            case 'a':
                return DeviceType.Hub;
            case 'b':
                return DeviceType.Node;
            default:
                Utils.printWrongInputMessage();
                return null;
        }
    }

    void createAlarm(){
        NetworkDevice device = null;
        DeviceType deviceType = getDeviceType();
        if (deviceType == null) return;

        if(deviceType.equals(DeviceType.Hub)){
            device = getHubFromUser();
        }
        else if(deviceType.equals(DeviceType.Node)){
            device = getNodeFromUser();
        }
        if(device == null) return;
        AlarmType alarmType = getAlarmFromUser();
        if(alarmType == null) return;
        device.addAlarm(alarmType);
        Utils.printYellow("Alarm successfully added.\n");
    }

    void viewAlarmStatus(){
        DeviceType deviceType = getDeviceType();
        if(deviceType == null){
            return;
        }
        System.out.println("Please input the IDs of the devices separated by a space.");
        scanner.nextLine();
        String input = scanner.nextLine();
        if(deviceType.equals(DeviceType.Hub)){
            controller.printAlarmStatusForHubs(input);
        }
        else{
            controller.printAlarmStatusForNodes(input);
        }
    }

    void viewSuggestedRemedies(){
        DeviceType deviceType = getDeviceType();
        if(deviceType == DeviceType.Hub){
            Hub hub = getHubFromUser();
            controller.viewSpecificRemediesForHubAlarms(hub);
        }
        else if(deviceType == DeviceType.Node){
            Node node = getNodeFromUser();
            controller.viewSpecificRemediesForNodeAlarms(node);
        }
    }

    void clearSpecificOrAllAlarms() {
        System.out.println("Would you like to clear..");
        System.out.println("    a)All alarms.");
        System.out.println("    b)Specific alarms.");
        System.out.print("\n>>");
        char choice = scanner.next().toLowerCase().charAt(0);
        DeviceType deviceType;
        NetworkDevice device;

        if (choice == 'a' || choice == 'b') {
            deviceType = getDeviceType();
            if(deviceType == DeviceType.Hub){
                device = getHubFromUser();
            }
            else if(deviceType == DeviceType.Node){
                device = getNodeFromUser();
            }
            else{
                return;
            }
            if(device == null) return;
            if(choice == 'a'){
                controller.clearAllAlarmsOnDevice(device);
            }
            else{
                AlarmType alarmType = getAlarmFromUser();
                if(alarmType == null)return;
                controller.clearSpecificAlarmOnDevice(device, alarmType);
            }
            System.out.println("Successfully cleared.\n");
            return;
        }
        Utils.printWrongInputMessage();
    }
}
