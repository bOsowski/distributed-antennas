package com.errigal;

import com.errigal.network.*;
import com.errigal.utils.Utils;
import com.google.gson.Gson;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class DataManager {

    public boolean saveToFile(String fileName, Controller controller){
        SimpleNetwork simpleNetwork = new SimpleNetwork();
        for(Carrier carrier: controller.network.getCarriers().values()){
            SimpleCarrier simpleCarrier = new SimpleCarrier(carrier.getName());
            simpleNetwork.carriers.add(simpleCarrier);
            for(Hub hub:carrier.getHubsByNames().values()){
                SimpleHub simpleHub = new SimpleHub(hub.getName(), hub.getID());
                simpleHub.alarms = hub.getAlarms();
                simpleCarrier.simpleHubs.add(simpleHub);
                for(Node node: hub.getNodesByNames().values()){
                    SimpleNode simpleNode = new SimpleNode(node.getName(), node.getID());
                    simpleNode.alarms = node.getAlarms();
                    simpleHub.simpleNodes.add(simpleNode);
                }
            }
        }

        Gson gson = new Gson();
        StringBuilder json = new StringBuilder();
               json.append(gson.toJson(simpleNetwork));
        try(PrintWriter printWriter = new PrintWriter(fileName)){
            try(BufferedWriter writer = new BufferedWriter(printWriter)){
                writer.write(json.toString());
                Utils.printYellow("Successfully saved to file.\n");
                return true;
            }
        }catch(Exception e){}
        return false;
    }

    public boolean readFromFile(String fileName, Controller controller){
        controller.network = new Network();
        Gson gson = new Gson();
        try(Scanner scanner = new Scanner(new File(fileName))){
            StringBuilder json = new StringBuilder();
            while(scanner.hasNext()){
                json.append(scanner.nextLine());
            }
            SimpleNetwork simpleNetwork = gson.fromJson(json.toString(), SimpleNetwork.class);
            for(SimpleCarrier simpleCarrier: simpleNetwork.carriers){
                controller.addCarrier(simpleCarrier.name);
                Carrier carrier = controller.getNetwork().getCarriers().get(simpleCarrier.name);
                for(SimpleHub simpleHub: simpleCarrier.simpleHubs){
                    controller.addHub(carrier, simpleHub.name, simpleHub.id);
                    Hub hub = controller.getNetwork().getHubsByIDs().get(simpleHub.id);
                    hub.loadAlarms(simpleHub.alarms);
                    for(Alarm alarm: simpleHub.alarms.values()){
                        hub.addAmountOfTotalAlarms(alarm.getAmount());
                    }

                    for(SimpleNode simpleNode: simpleHub.simpleNodes){
                        controller.addNode(hub, simpleNode.name, simpleNode.id);
                        Node node = controller.getNetwork().getNodesByIDs().get(simpleNode.id);
                        node.loadAlarms(simpleNode.alarms);
                        for(Alarm alarm: simpleNode.alarms.values()){
                            node.addAmountOfTotalAlarms(alarm.getAmount());
                        }
                    }
                }
            }
            Utils.printYellow("Successfully imported the network from a file.\n");
            return true;
        }catch (Exception e){}
        return false;
    }

    class simpleNetworkDeice{
        String name;
        String id;
        HashMap<AlarmType, Alarm> alarms;

        simpleNetworkDeice(String name, String id){
            this.name = name;
            this.id = id;
        }
    }

    class SimpleHub extends simpleNetworkDeice{
        ArrayList<SimpleNode> simpleNodes = new ArrayList<>();


        SimpleHub(String name, String id) {
            super(name, id);
        }
    }

    class SimpleNode extends simpleNetworkDeice{

        SimpleNode(String name, String id) {
            super(name, id);
        }
    }

    class SimpleCarrier{
        String name;
        ArrayList<SimpleHub> simpleHubs = new ArrayList<>();

        SimpleCarrier(String name){
            this.name = name;
        }
    }

    class SimpleNetwork{
        ArrayList<SimpleCarrier> carriers = new ArrayList<>();
    }

}
