#### Advanced Java Assessment - Distributed Antennas

##### Things to make note of
* Main.java contains the entry point of the application (main class).
* The exporting and importing doesn't ask to specify the file, just saves/loads the file "DataSave.json" to/from the current working directory.
* Be careful when exporting the network, since it won't ask for a prompt and will override the existing file.
* Also be careful when importing the network, since any of the unsaved data will be replaced with the data from the file.
* To leave the current inner menu just input an invalid choice.

###### Author: Bartosz Osowski

